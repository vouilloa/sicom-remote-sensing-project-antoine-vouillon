# Remote sensing course SICOM 3A - Project

## 1. Introduction

As a final exam, this project aims to evaluate 3A SICOM students. 

Students will work individually, and each student must submit its work (report and code) as a merge request on Gitlab by **Sunday, January 14th**.

## 2. Presentation

In many remote sensing applications it is impossible to obtain a high spatial *and* spectral resolution due to physical constraints. A common way to solve this problem is to use multiple sensors that will either focus on the spatial details, while others will focus on spectral details. Here we propose to fuse the data comming from 3 different sensors, where each sensor as different spatial and spectral resolutions. We provide the characterization of those sensors in the folder `data`, with `data/img.py`, an hyperspectral image that will serve as ground truth for the project:

![alt text](readme_imgs/img.png)

The first sensor is taken from the QuickBird sensor. It is panchromatic, meaning it will only acquire gray-scale images by integrating over all the spectrum, but with high spatial details. Such a sensor will thus produce images with almost no spectral information but with a very good spatial resolution. Here is the spectral response of the sensor:

![alt text](readme_imgs/pan.png)

The second sensor is a modified version of the one used in WorldView-3. It as 9 spectral bands but as half the spatial resolution of the panchromatic one. Here are the spectral responses of the sensor:

![alt text](readme_imgs/wv3.png)

Finally the last sensor as "perfect" spectral resolution (meaning it conserves all the spectral resolution of the ground truth) but is 4 times less efficient in terms of spatial resolution.

We propose you to reconstruct the ground truth using the 3 differents acquisitions. Of course using directly the ground truth is forbiden, it can only be used to compute metrics (SSIM, PSNR).

## 3. How to proceed?

The image you will work on is Washington DC Mall, a $307 \times 1280 \times 99$ array. This image was acquired by the HYDICE sensor, see references. You have to use methods seen during this semester to recover a spatial and spectral image using **only** the three acquisitions (or degraded images). We provide you the 3 degradation operators (one for each sensor) to compute the acquisitions from the ground truth in case you want to use different images.

## 4. Some hints to start

We provide in `src/methods/baseline` a basic upscaling image which can help you to start. You can also find in the academic litterature techniques (interpolation, inverse problem, machine learning, etc) that solves problems close to this one.

*The time of computations might be very long depending of your machine. To verify that your method works, try to work with smaller test images.*

## 5. Project repository

The project has the following organization: 

~~~text
sicom_image_analysis_project/
├─.gitignore                   # Git's ignore file
├─data/                        # All the data provided (image, spectral responses, etc)
├─main.ipynb                   # A notebook to experiment with the code
├─output/                      # The output folder where you can save your reconstruction
├─README.md                    # Readme, contains all information about the project
├─readme_imgs/                 # Images for the Readme
├─requirements.txt             # Requirement file for packages installation
└─src/                         # All of the source files for the project
  ├─checks.py                  # File containing some sanity checks
  ├─forward_model.py           # File containing the CFA operator
  ├─utils.py                   # Some utilities
  └─methods/
    ├─baseline/                # Example of reconstruction
    └─template/                # Template of your project (to be copied)
~~~

## 6. Instructions:

Each student will fork this Git repository on its own account. You will then work in your own version of this repository.

Along with your code you must submit a report **as a pdf file** with the name **name.pdf** inside the folder `src/methods/your_name`.

The code and the report must be **written in English**.

### 6.1. The code:

- You should first **copy** the folder `src/methods/template` and rename it with your name. It is in this folder that you will work, **nothing else should be modified** apart from `main.ipynb` for experimenting with the code.
- You will add as many files you want in this folder but the only interface to run your code is `run_reconstruction` in `src/methods/your_name`. You can modify this function as you please, as long as it takes in argument the panchromatic image (`pan`), the mid resolution multispectral image (`ms1`), the low resolution multispectral image (`ms2`) and returns a single image.
- Have a look in `src/methods/baseline`, your projet should work in the same way.
- You can use all the functions defined in the project, even the ones that you should not modify (`utils.py`, `degradation_operators.py`, etc).
- Your code must be operational through `run_reconstruction`, as we will test it on new and private images.
- The notebook provides a working bench. It should **not be included in the merge request**, it is just a work document for you.
- Comment your code when needed.

### 6.2. The report:

Your report **must be a pdf file** written in English and should not be longer than 5 pages. In this file you are asked to explain:

- The problem statement, show us that you've understood the project.
- The solution that you've chosen, explaining the theory.
- With tools that **you've built** show us that your solution is relevant and working
- Results.
- Conclusion, take a step back about your results. What can be improved?

### 6.3. Submission:

To submit your work you just need to create a merge request in Gitlab (see [here](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork) for a detailed explanation). This merge request will **only encompass the changes made to `src/methods/your_name`, without `main.ipynb`**. Because the merge request will only create `src/methods/your_name` there will not be any conflicts between each student's project.

## 7. Supervisors

- Mauro Dalla Mura: mauro.dalla-mura@gipsa-lab.grenoble-inp.fr
- Matthieu Muller: matthieu.muller@gipsa-lab.grenoble-inp.fr

## 8. References

- HYDICE sensor: [website](https://ntrs.nasa.gov/citations/19950027312)
- WorldView-3: [website](https://earth.esa.int/eogateway/missions/worldview-3)
- QuickBird: [website](https://earth.esa.int/eogateway/catalog/quickbird-full-archive)